const CFG = {
	module: 'pf1-token-tooltip',
	SETTINGS: {
		sticky: 'sticky',
	}
};

let lastToken, lastActor, currentHUD;

const WoundThreshold = {
	0: 'Healthy',
	1: 'Grazed',
	2: 'Wounded',
	3: 'Critical',
};

/**
 * @param {String} element
 * @param {Object} options
 */
function createNode(element, { text, html, css = [], id, name } = {}) {
	const el = document.createElement(element);
	if (text) el.textContent = text;
	else if (html) el.innerHTML = html;
	if (css.length) el.classList.add(...css);
	if (id) el.id = id;
	if (name) el.name = name;
	return el;
}

class Point {
	x;
	y;
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}

class ValueElement {
	element;
	label;
	value;
	constructor(label, value, options) {
		this.element = createNode('div', options);
		this.label = createNode('label', { text: label, css: ['label'] });
		this.value = createNode('label', { text: value, css: ['value'] });
		this.element.append(this.label, this.value);
	}
}

class Tooltip {
	/** @type {Tooltip} */
	static instance;

	/** @type {Element} */
	tooltip;

	name = createNode('h3', { id: 'alt-token-tooltip-name', css: ['actor-name', 'enhanced-tooltip-block'] });

	health = {
		/** @type {Element} */
		element: undefined,
		normal: new ValueElement('Health', '0', { css: ['normal', 'row'] }),
		nonlethal: new ValueElement('Nonlethal', '0', { css: ['nonlethal', 'row'] }),
		temporary: new ValueElement('Temporary', '0', { css: ['temporary', 'row'] }),
		state: createNode('label', { css: ['health-state'] }),
	};

	armor = {
		/** @type {Element} */
		element: undefined,
		armor: new ValueElement('AC', '0', { css: ['normal', 'row'] }),
		touch: new ValueElement('Touch', '0', { css: ['touch', 'row'] }),
		cmd: new ValueElement('CMD', '0', { css: ['cmd', 'row'] }),
	};

	dcs = {
		/** @type {Element} */
		element: undefined,
		fort: new ValueElement('Fort', '0', { css: ['fort', 'row'] }),
		ref: new ValueElement('Ref', '0', { css: ['ref', 'row'] }),
		will: new ValueElement('Will', '0', { css: ['will', 'row'] }),
		intimidate: new ValueElement('Intimidate', '0', { css: ['intimidate', 'row'] }),
		feint: new ValueElement('Feint', '0', { css: ['feint', 'row'] }),
	};

	mobility = {
		/** @type {Element} */
		element: undefined,
		land: new ValueElement('Land', '0', { css: ['land', 'row'] }),
		climb: new ValueElement('Climb', '0', { css: ['climb', 'row'] }),
		swim: new ValueElement('Swim', '0', { css: ['swim', 'row'] }),
		burrow: new ValueElement('Burrow', '0', { css: ['burrow', 'row'] }),
		fly: new ValueElement('Fly', '0', { css: ['fly', 'row'] }),
	};

	/** @type {Element} */
	nameBox;
	/** @type {Element} */
	rightBox;
	/** @type {Element} */
	leftBox;

	constructor() {
		this.tooltip = createNode('template', { id: 'alt-token-tooltip' });

		// HEALTH
		this.health.element = createNode('div', { id: 'alt-token-tooltip-health', css: ['enhanced-tooltip-block'] });
		this.health.element.append(this.health.normal.element, this.health.nonlethal.element, this.health.temporary.element, this.health.state);

		// Armor
		this.armor.element = createNode('div', { id: 'alt-token-tooltip-armor', css: ['enhanced-tooltip-block'] });
		this.armor.element.append(this.armor.armor.element, this.armor.touch.element, this.armor.cmd.element);

		// DCs
		this.dcs.element = createNode('div', { id: 'alt-token-tooltip-dcs', css: ['enhanced-tooltip-block'] });
		this.dcs.element.append(this.dcs.fort.element, this.dcs.ref.element, this.dcs.will.element, this.dcs.intimidate.element, this.dcs.feint.element);

		// MOBILITY
		this.mobility.element = createNode('div', { id: 'alt-token-tooltip-mobility', css: ['enhanced-tooltip-block'] });
		this.mobility.element.append(this.mobility.land.element, this.mobility.climb.element, this.mobility.swim.element, this.mobility.burrow.element, this.mobility.fly.element);

		// OTHER
		// ...

		this.nameBox = createNode('div', { id: 'alt-token-tooltip-name', css: ['actor-name-box', 'enhanced-tooltip-context'] });
		this.nameBox.append(this.name);

		this.rightBox = createNode('div', { id: 'alt-token-tooltip-right-panel', css: ['enhanced-tooltip-context'] });
		this.rightBox.append(this.health.element, this.mobility.element);

		this.leftBox = createNode('div', { id: 'alt-token-tooltip-left-panel', css: ['enhanced-tooltip-context'] });
		this.leftBox.append(this.armor.element, this.dcs.element);

		this.tooltip.append(this.nameBox, this.rightBox, this.leftBox);

		Tooltip.instance = this;

		document.body.append(this.tooltip);
	}

	/**
	 * @param {Token} token
	 */
	setPosition(token) {
		const tokenWidth = token.w * canvas.stage.scale.x,
			tokenHeight = token.h * canvas.stage.scale.y,
			left = Math.round(token.worldTransform.tx),
			// right = window.innerWidth - Math.round(token.worldTransform.tx),
			top = Math.round(token.worldTransform.ty),
			right = left + tokenWidth,
			bottom = top + tokenHeight;

		this.tooltip.style.setProperty('--left', left);
		this.tooltip.style.setProperty('--top', top);
		this.tooltip.style.setProperty('--right', right);
		this.tooltip.style.setProperty('--bottom', bottom);
	}

	/**
	 * @param {Token} token
	 * @param {Actor} actor
	 */
	setContent(token, actor) {
		this.name.textContent = token.name ?? actor?.name ?? '¿ Unnamed ?';

		if (!actor) {
			this.health.state.textContent = '';
			return;
		}

		this.lastActor = actor;
		this.lastToken = token;

		const actorData = actor.data.data;

		// Basic health
		const attributes = actorData.attributes,
			abilities = actorData.abilities,
			hp = attributes.hp,
			hpEffective = hp.value + hp.temp,
			hpEffectiveMax = Math.max(hp.max, hpEffective),
			pctHp = Math.roundDecimals(hp.value / hp.max * 100, 1);

		this.health.normal.value.textContent = `${hp.value} / ${hp.max} [${pctHp}%]`;
		let nlStr = `${hp.nonlethal}`;
		if (hp.nonlethal > 0 && hp.nonlethal > hpEffective) nlStr += ' [Unconscious]';
		this.health.nonlethal.value.textContent = nlStr;
		this.health.temporary.value.textContent = `${hp.temp}`;

		// Wound Threshold
		const wt = 4 - Math.ceil(Math.clamped(hpEffective / hp.max, 0, 1) * 4);
		this.health.state.dataset.level = wt;

		// Build Health Statemenet
		let str;
		if (Number.isFinite(pctHp)) {
			str = WoundThreshold[wt] ?? 'Dying';

			if (wt > 0) str += ` (${-wt})`;
			str += ` [≤${(4 - wt) * 25}%]`;

			if (hpEffective <= hp.nonlethal) {
				if (hpEffective === hp.nonlethal)
					str += ' [Staggered]';
				else if (hpEffective <= (-attributes[attributes.hpAbility]?.total ?? 0))
					str += ' [DEAD]';
				else
					str += ' [Unconscious]';
			}
		}
		else
			str = '';

		this.health.state.textContent = str;

		// Mobility

		for (const spd of ['land', 'climb', 'swim', 'burrow', 'fly']) {
			const baseSpeed = attributes.speed[spd].total;
			const [speed, unit] = pf1.utils.convertDistance(baseSpeed, 'ft');
			this.mobility[spd].value.textContent = `${speed} ${unit} [${Math.floor(baseSpeed / 5)} cells]`;
			this.mobility[spd].element.classList.toggle('hidden', baseSpeed == 0 && spd !== 'land');
		}

		// Defenses
		this.armor.armor.value.textContent = `${attributes.ac.normal.total}`;
		this.armor.touch.value.textContent = `${attributes.ac.touch.total}`;
		this.armor.cmd.value.textContent = `${attributes.cmd.total}`;

		this.dcs.fort.value.textContent = `${attributes.savingThrows.fort.total}`;
		this.dcs.ref.value.textContent = `${attributes.savingThrows.ref.total}`;
		this.dcs.will.value.textContent = `${attributes.savingThrows.will.total}`;
		this.dcs.intimidate.value.textContent = `${10 + Math.floor(attributes.hd.total / 2)}`;
		this.dcs.feint.value.textContent = `${10 + Math.max(abilities.wis.mod, actorData.skills.sen.mod)}`;
	}

	/**
	 * Adjust content layout to make it pretty.
	 */
	adjustContent() {
		this.leftBox.style.setProperty('--width', this.leftBox.clientWidth);
	}

	toggle(enabled) {
		if (enabled && currentHUD?.rendered) return; // Don't render if hud is up
		this.tooltip.classList.toggle('active', enabled);
		if (enabled) this.refresh();
	}

	refresh() {
		const token = this.lastToken;
		if (token) this.setContent(token, token.actor);
	}

	isSame(token) {
		return this.lastToken.id === token.id;
	}

	/**
	 *
	 * @param {Token} token
	 * @param {Actor} actor
	 */
	static show({ token, actor } = {}) {
		const tooltip = Tooltip.instance;
		lastToken = token;
		lastActor = actor ?? token.actor;
		tooltip.setPosition(token);
		tooltip.setContent(token, actor);
		tooltip.toggle(true);
		// Adjust needs to be done after render or the size data is wrong
		tooltip.adjustContent();
	}

	static hide(scrapLast = true) {
		if (scrapLast) lastActor = lastToken = undefined;
		Tooltip.instance.tooltip.classList.remove('active');
	}
}

/**
 * @param {Token} token
 * @param {Boolean} hovering
 */
function onTokenHover(token, hovering) {
	const actor = token?.actor;

	// Ignore hover events triggered by non-hover mouse activity (e.g. dragging)
	const state = canvas.currentMouseManager?.state ?? 1;
	if (state !== 1) return;

	if (!actor || actor.type === 'basic') return Tooltip.hide();

	if (hovering && (actor?.testUserPermission(game.user, 'OBSERVER') === true || game.user.isGM))
		Tooltip.show({ token, actor });
	else {
		if (!game.settings.get(CFG.module, CFG.SETTINGS.sticky))
			Tooltip.hide();
	}
}

function onCanvasPanDelayed() {
	// Adjust tooltip as long as it's hovering
	if (lastToken) {
		Tooltip.instance.setPosition(lastToken, true);
		Tooltip.instance.toggle(true);
	}
}

const canvasPanDebounce = foundry.utils.debounce(onCanvasPanDelayed, 100);

function onDeleteToken(token, options, userId) {
	if (token.parent?.id !== lastToken?.scene.id) return;
	if (token.id === lastToken.id) Tooltip.hide();
}

/**
 * @param {TokenDocumnet} token
 * @param {Object} update
 * @param {Object} options
 * @param {String} userId
 */
function onTokenUpdate(token, update, options, userId) {
	if (token.parent?.id !== lastToken?.scene.id) return;

	// No movement
	if (update.x === undefined && update.y === undefined) return;

	// Different token
	if (token.id !== lastToken.id) return Tooltip.hide();
	else Tooltip.hide(false);

	CanvasAnimation.getAnimation(token.object.movementAnimationName).promise
		.then(_ => {
			Tooltip.instance.setPosition(token.object);
			Tooltip.instance.toggle(true);
		});
}

function onActorUpdate(actor, update) {
	if (Tooltip.instance.lastToken?.actor?.id === actor.id)
		Tooltip.instance.refresh();
}

function onCanvasPan() {
	Tooltip.instance.toggle(false);
	canvasPanDebounce();
}

function onCanvasInit() {
	Tooltip.hide();
}

function onRenderTokenHud(tokenhud) {
	console.log('onRenderTokenHud');
	currentHUD = tokenhud;
	Tooltip.hide();
}

// Following are only needed with sticky hover
const stickyTokenEvents = {
	canvasPan: onCanvasPan,
	canvasInit: onCanvasInit,
};

function toggleStickyHover(sticky) {
	console.log('PF1 Alt Token Tooltip; Sticky Hover:', sticky);
	for (const [event, fn] of Object.entries(stickyTokenEvents)) {
		if (sticky) Hooks.on(event, fn);
		else Hooks.off(event, fn);
	}

	if (!sticky) Tooltip.instance.hide();
}

Hooks.once('init', function setup() {
	game.settings.register(CFG.module, CFG.SETTINGS.sticky, {
		name: 'Sticky',
		hint: 'Keep tooltip out even with hover out.',
		default: false,
		type: Boolean,
		config: true,
		onChange: toggleStickyHover,
		scope: 'client',
	});
});

Hooks.once('ready', function initialize() {
	new Tooltip();

	Hooks.on('hoverToken', onTokenHover);

	Hooks.on('updateToken', onTokenUpdate);
	Hooks.on('updateActor', onActorUpdate);
	Hooks.on('deleteToken', onDeleteToken);

	Hooks.on('renderTokenHUD', onRenderTokenHud);

	if (game.settings.get(CFG.module, CFG.SETTINGS.sticky))
		toggleStickyHover(true);
});
