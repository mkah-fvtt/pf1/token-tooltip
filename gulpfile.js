const project = 'token-tooltip';
const gulp = require('gulp');
const through2 = require('through2')

function updateDownloadPath() {
	return gulp.src('./module.json')
		.pipe(
			through2.obj((file, _, cb) => {
				const string = file.contents.toString();
				const re = string.match(/"version": "(?<ver>[^"]+)"?,$/m);
				const version = re?.groups.ver;
				if (!version) throw new Error('Version not found in module.json');
				// Replace version in download string
				let sameVer = 0;
				const modified = string.replace(/(?=[^"])\d+\.\d+(\.\d+)?(\.\d+)?(?!\.?\d+)(?!")/gm, (m) => {
					if (m === version) sameVer++;
					return version;
				});
				if (sameVer > 1) {
					console.log('module.json is up to date');
					cb(null);
				}
				else {
					console.log('module.json download path needs updating');
					file.contents = Buffer.from(modified);
					cb(null, file)
				}
			})
		)
		.pipe(gulp.dest('./'));
}

gulp.task('release', gulp.parallel(updateDownloadPath));
